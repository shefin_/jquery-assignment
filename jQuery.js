$(document).ready(function() {

    let updateCalledBy, updateCallerName;

    $("#home-btn").click(genIndexPage);
    $("#create-place").click(genFormPage);
    $("#submit-form").click(submitForm);
    $(".up-arrow, .down-arrow").click(sortItems);
    $("#search-text-box").on('input', performSearch);

    
    function hideIndexPage() {
        $('#index-page-div').hide();
    }
    
    function showIndexPage() {
        $(document).attr("title", 'Tourist Places');
        $('#index-page-div').show();
    }
    
    function hideFormPage() {
        $('#form-page-div').hide();
    }
    
    function showFormPage() {
        $(document).attr("title", 'Add a new Tourist Place');
        $('#form-page-div').show();
    }
    
    function genIndexPage() {
        $('.deletable-form').remove();
    
        updateCalledBy = undefined;
        updateCallerName = undefined;
    
        hideFormPage()
        showIndexPage()
    }
    
    function genFormPage() {
        hideIndexPage()
        showFormPage()
    }
    
    function doDelete() {
        if(!confirm("The Tourist Place Will Be Deleted!!")) return;
        $(this).closest('.row-list').remove();
    }
    
    function doUpdate() {
        updForm = $('.form-tag')[0];
        updateCalledBy = this;
        
        let rowContainer = $(this).closest('.row-list');
        let divChildren = $(rowContainer).find("div");
    
        for(child of divChildren) {
            if($(child).attr('class') == 'action')
                continue;
    
            if($(child).attr('class') == 'picture') {
                childClass = $(child).attr('class');
                imgFieldSpan = $(updForm).find(`[name=${childClass}]`).val('').next();
    
                let spanElement = $("<span>").addClass("deletable-form");
                $(imgFieldSpan).after(spanElement);
    
                let imgTag = $(child).children(":first");
                let imgDiv = createPictureDiv($(imgTag).attr("src")).css("Float", "left")
                                                        .addClass("deletable-form img-preview");
                $(imgFieldSpan).after(imgDiv);
    
                let labelElement = $("<label>").text("Old Picture:")
                                                .addClass("deletable-form");
                $(imgFieldSpan).after(labelElement);
    
                continue;
            }
    
            updForm.elements[$(child).attr('class')].value = $(child).text().trim();
    
            if($(child).attr('class') == 'name') updateCallerName = $(child).text().trim();
        }
    
        genFormPage();
    }
    
    function createNameDiv(name) {
        let flag = true;
        $('.name').each(function() {
            let placeName = $(this).text().trim();
            if(placeName == name && placeName != updateCallerName) 
                return flag = false;
        });
        if(!flag) return null;
    
        return $('<div>').addClass("name").text(`${name}`);
    }
    
    function createAddressDiv(address) {
        return $('<div>').addClass("address").text(`${address}`);
    }
    
    function createRatingDiv(rating) {
        return $('<div>').addClass("rating").text(`${rating}`);
    }
    
    function createPictureDiv(img) {
        divElement = $('<div>').addClass("picture")
    
        imgElement = $("<img>").attr("src", `${img}`)
                               .animate({
                                    height: "200",
                                    width: "150"
                               });
    
        $(divElement).append(imgElement);
        return divElement;
    }
    
    function createButtonsDiv() {
        divElement = $('<div>').addClass("action")
    
        spanElement = $("<span>");
    
        buttonUpdate = $("<button>").attr("type", "button")
                                    .addClass("btn-blue")
                                    .text("Update")
                                    .click(doUpdate);
    
        buttonDelete = $("<button>").attr("type", "button")
                                    .addClass("btn-red")
                                    .text("Delete")
                                    .click(doDelete);
    
        $(spanElement).append(buttonUpdate);
        $(spanElement).append(" ");
        $(spanElement).append(buttonDelete);
    
        $(divElement).append(spanElement);
        return divElement;
    }
    
    function getImageForm(fileField) {
        if($(fileField).prop('files').length == 0)
            return null;
    
        const dir = "upload/";
        return dir + $(fileField).prop('files')[0].name;
    }
    
    function submitForm(event) {
        event.preventDefault();
    
        formNew = $('.form-tag')[0];
    
        const name = $(formNew).find("[name='name']").val().trim();
        const address = $(formNew).find("[name='address']").val().trim();
        const rating =  $(formNew).find("[name='rating']").val();
        const type = $(formNew).find("[name='type']").val();
        let img = getImageForm($(formNew).find("[name='picture']"));
        // const img = newImg;
    
        if(name) {
            $(formNew).find("[name='name']").next().html('');
            $(formNew).find("[name='name']").css("border-color", "black");
        }
    
        if(address) {
            $(formNew).find("[name='address']").next().html('');
            $(formNew).find("[name='address']").css("border-color", "black");
        }
    
        if(rating) {
            $(formNew).find("[name='rating']").next().html('');
            $(formNew).find("[name='rating']").css("border-color", "black");
        }
    
        if(type) {
            $(formNew).find("[name='type']").next().html('');
            $(formNew).find("[name='type']").css("border-color", "black");
        }
    
        if(img || updateCalledBy) {
            $(formNew).find("[name='picture']").next().html('');
            $(formNew).find("[name='picture']").css("border-color", "black");
        }
    
        if(!name || !address || !rating || !type || (!img && !updateCalledBy)) {
    
            if(!name) {
                $(formNew).find("[name='name']").next()
                                                .html("<b> *Required </b>")
                                                .css("color", "red");
    
                $(formNew).find("[name='name']").css("border-color", "red");
            }
    
            if(!address) {
                $(formNew).find("[name='address']").next()
                                                    .html("<b> *Required </b>")
                                                    .css("color", "red");
    
                $(formNew).find("[name='address']").css("border-color", "red");
            }
    
            if(!rating) {
                $(formNew).find("[name='rating']").next()
                                                    .html("<b> *Required </b>")
                                                    .css("color", "red");
    
                $(formNew).find("[name='rating']").css("border-color", "red");
            }
    
            if(!type) {
                $(formNew).find("[name='type']").next()
                                                .html("<b> *Required </b>")
                                                .css("color", "red");
    
                $(formNew).find("[name='type']").css("border-color", "red");
            }
    
            if(!img && !updateCalledBy) {
                $(formNew).find("[name='picture']").next()
                                                   .html("<b> *Required </b>")
                                                   .css("color", "red");
    
                $(formNew).find("[name='picture']").css("border-color", "red");
            }
    
            alert("Please Fill Up All The Fields!");
            return;
        }
        
        if(Number(rating) < 1 || Number(rating) > 5) {
            alert("Rating Must Be In [1,5] Range");
            return;
        }
    
        divRow = $("<div>").addClass("row-list");
    
        nameDiv = createNameDiv(name);
        if(nameDiv == null) {
            alert("Tourist Places' Names Should Be Unique !");
            return;
        }
        
        if(updateCalledBy) {
            updateForm();
            return;
        }
    
        $(divRow).append(nameDiv);
        $(divRow).append(createAddressDiv(address));
        $(divRow).append(createRatingDiv(rating));
        $(divRow).append(createPictureDiv(img));
        $(divRow).append(createButtonsDiv());
    
        divContainer = $('.container-list')[0];
        $(divContainer).append(divRow);
    
        genIndexPage();
    }
    
    function updateForm() {
        formNew = $('.form-tag')[0];
    
        const name = $(formNew).find("[name='name']").val().trim();
        const address = $(formNew).find("[name='address']").val().trim();
        const rating =  $(formNew).find("[name='rating']").val();
        const type = $(formNew).find("[name='type']").val();
        let img = getImageForm($(formNew).find("[name='picture']"));
        // const img = newImg;
    
        let elem = updateCalledBy;
        let rowContainer = $(elem).closest('.row-list');
        let divChildren = $(rowContainer).find("div");
        let imgTag = $(rowContainer).find('img')[0];
    
        if(!img) img = $(imgTag).attr("src");
    
        function applyUpdate(elem) {
    
            for(child of divChildren) {
                if($(child).hasClass('action'))
                    continue;
    
                if($(child).hasClass('picture')) {
                    img_child = $(child).children()[0];
                    $(img_child).attr("src", img);
                    continue;
                }
    
                $(child).text( $(formNew).find(`[name=${ $(child).attr('class') }]`).val() );
            }
        }
    
        applyUpdate(updateCalledBy);
        genIndexPage();
    }
    
    function sortContents(criteria, ascending=true) {
        rows = $('.row-list');
        parentElement = $(rows[0]).parent();
    
        araRows = Array.from(rows);
        araRows.shift(); // 0th row is the header.
    
        function comp(item1, item2) {
            val1 = $( $(item1).find('.'+criteria)[0] ).text();
            val2 = $( $(item2).find('.'+criteria)[0] ).text();
    
            if(val1 > val2) {
                return ascending? 1 : -1;
            }
            else if(val1 == val2)
                return 0;
            else {
                return ascending? -1 : 1;
            }
        }
    
        araRows.sort(comp);
    
        for(row of araRows) {
            $(parentElement).append(row);
        }
    }
    
    function sortItems() {
        let arrowType = $(this).attr("class").trim();
        let criteria = $(this).parent().text().trim().toLowerCase();
        
        if(arrowType == 'down-arrow') sortContents(criteria, true);
        else sortContents(criteria, false);
    }
    
    function searchMatch(searchText, placeName) {
        if(!searchText) return true;
    
        searchText = searchText.toLowerCase();
        placeName = placeName.toLowerCase();
    
        return placeName.indexOf(searchText) != -1;
    }
    
    function performSearch() {
        searchText = $(this).val().trim();
    
        places = $('.row-list');
        let odd = 0;

        $('.row-list').each(function(i) {
            if(i==0) return;  // i=0 is the header

            placeName = $( $(this).find('.name')[0] ).text().trim();

            if(!searchMatch(searchText, placeName)) {
                $(this).hide();
            }
            else {
                $(this).show();
            }
        });
    }
});
